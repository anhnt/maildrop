terraform {
  backend "s3" {
    bucket = "maildrop.terraform"
    key    = "state"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

variable "app_image" {
  default = "mark242/maildrop-smtp:latest"
}

variable "key_name" {
  default = "awshosts-west"
}

variable "domain" {
  default = "maildrop.cc"
}

variable "altinbox_mod" {}
